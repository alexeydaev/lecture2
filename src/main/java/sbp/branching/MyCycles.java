package sbp.branching;

import sbp.common.Utils;

public class MyCycles
{
    private final Utils utils;

    public MyCycles(Utils utils)
    {
        this.utils = utils;
    }

    /**
     * Необходимо написать реализацию метода с использованием for()
     +     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     +     * Реализация Utils#utilFunc1() неизвестна
     +     * Должна присутствовать проверка возврщаемого значения от Utils#utilFunc1()
     * Результат проверяется через unit-test (все тесты должны выполниться успешно)
     * @param iterations - количество итераций
     * @param str - строка для вывода через утилиту {@link Utils}
     */
    public void cycleForExample(int iterations, String str)
    {
        /*
        Реализация цикла FOR
       Вызываем utilFunc1() на каждой итерации = 2
       Проверяем возвращаемое значение utilFunc1() на true/false
       В случае - true, выводим текст в консоль
         */
        for (int i = 0;i<iterations;i++){
            if (utils.utilFunc1(str)) {
                System.out.println(str);
            }
        }
    }

    /**
     * Необходимо написать реализацию метода с использованием while()
     +     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     +     * Реализация Utils#utilFunc1() неизвестна
     +     * Должна присутствовать проверка возврщаемого значения от Utils#utilFunc1()
     * Результат проверяется через unit-test (все тесты должны выполниться успешно)
     * @param iterations - количество итераций
     * @param str - строка для вывода через утилиту {@link Utils}
     */
    public void cycleWhileExample(int iterations, String str)
    {
        /*
        Реализация цикла WHILE
       Вызываем utilFunc1() на каждой итерации = 3, но цикл должен отработать 1 раз
       Проверяем возвращаемое значение utilFunc1() на true/false
       В случае - true, выводим текст в консоль
       Переменной iterations присваиваем 0 для завершения проверки
         */
        while (iterations>0){
            if (utils.utilFunc1(str)) {
                System.out.println(str);
            }
            iterations = 0;
        }
    }

    /**
     * Необходимо написать реализацию метода с использованием while()
     -     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     -     * Реализация Utils#utilFunc1() неизвестна
     +     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     +     * Реализация Utils#utilFunc1() неизвестна
     * Должна присутствовать проверка возврщаемого значения
     * Результат проверяется через unit-test (все тесты должны выполниться успешно)
     * @param from - начальное значение итератора
     * @param to - конечное значение итератора
     * @param str - строка для вывода через утилиту {@link Utils}
     */
    public void cycleDoWhileExample(int from, int to, String str)
    {
        /*
        Реализация цикла DO WHILE
        Запускае функцию utilFunc1() хотябы 1 раз
        Присваиваем переменной to = 1
        проверка условия while(to < from) возвращает - false
         */
        do {
            if (utils.utilFunc1(str)) {
                System.out.println(str);
            }
            to = 3;
        }while (to < from);
    }
}

