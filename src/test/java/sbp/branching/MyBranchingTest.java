package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;
import sbp.branching.MyBranching;

import static org.mockito.ArgumentMatchers.anyString;

public class MyBranchingTest {
     /**
     *Данный тест проверяет результат выполнения метода {@link Utils}
     * в зависимости от переданных значение (возвращает болшее)
     * @value a - первое значение
     * @value b - второе значение
     */
    @Test
    public void branchingMaxInt()
    {
        int a = 10, b = 20;
        Utils utilsMock = Mockito.mock(Utils.class);

        MyBranching myBranching = new MyBranching(utilsMock);
        myBranching.maxInt(a,b);

        Assertions.assertTrue(b > a);
    }
    /**
     *Данный тест проверяет результат выполнения метода {@link Utils}
     * в зависимости от  mock на объект {@link Utils} возвращающий TRUE
     */
    @Test
    public void branchingTrueFalse()
    {
        int a = 10, b = 20;
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        Assertions.assertTrue(utilsMock.utilFunc2());
    }
    /**
     *Данный тест проверяет результат выполнения метода {@link Utils}
     * в зависимости от mock реализации возвращающей TRUE
     */
    @Test
    public void ifElseExample_Test_True(){
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);
        boolean isResult = myBranching.ifElseExample();

        Assertions.assertTrue(isResult);

    }
    /**
     *Данный тест проверяет результат выполнения метода {@link Utils}
     * в зависимости от mock реализации возвращающей FALSE
     */
    @Test
    public void ifElseExample_Test_false(){
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsMock);
        boolean isResult = myBranching.ifElseExample();

        Assertions.assertFalse(isResult);

    }
    /**
     * Тест содержать mock на объект {@link Utils} (возвращает TRUE)
     * Необходимо проверить выполнение Utils#utilFunc1 и Utils#utilFunc2 при
     * @value input - входящее значение (0)
     */
    @Test
    public void switchExample1(){
        Utils utilsMock = Mockito.mock(Utils.class);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Mockito.when(utilsMock.utilFunc1("abc2")).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsMock);

        int input = 0;
        myBranching.switchExample(input);
       Assertions.assertEquals(utilsMock.utilFunc2(),utilsMock.utilFunc1("abc2"));
    }
    /**
     * Тест содержать mock на объект {@link Utils} (возвращает TRUE)
     * Необходимо проверить выполнение Utils#utilFunc1 и Utils#utilFunc2 при
     * @value input - входящее значение (2)
     */
    @Test
    public void switchExample2(){
        Utils utilsMock = Mockito.mock(Utils.class);

        MyBranching myBranching = new MyBranching(utilsMock);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        int input = 2;
        myBranching.switchExample(input);
        Assertions.assertTrue(utilsMock.utilFunc2());
    }
}
